/* eslint-env node */
'use strict';

const request = require('request');

const proxyPath = '/token';

module.exports = function (app) {
    // For options, see:
    // https://github.com/nodejitsu/node-http-proxy
    // let proxy = require('http-proxy').createProxyServer({
    //     target: 'https://login.microsoftonline.com/common/oauth2/v2.0',
    //     secure: false,
    //     ssl: {
    //         key: fs.readFileSync('ssl/server.key'),
    //         cert: fs.readFileSync('ssl/server.crt')
    //     }
    // });

    app.use(proxyPath, function (req, res, next) {
        // include root path in proxied request
        // req.url = 'https://login.microsoftonline.com/common/oauth2/v2.0/token';
        // proxy.web(req, res, {
        //     target: 'https://login.microsoftonline.com/common/oauth2/v2.0/token'
        // });

        const url = 'https://login.microsoftonline.com/common/oauth2/v2.0/token';
        request.post(url, {
            form: req.body
        }, function optionalCallback(err, httpResponse, body) {
            if (err) {
                return res.status(500).json(body);
            }
            return res.json(body);
        });
    });
};
