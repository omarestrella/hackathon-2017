import AuthService from 'hackathon/services/auth';

export function initialize(application) {
    application.register('service:auth', AuthService, { initialize: true });
}

export default {
    name: 'auth',
    initialize
};
