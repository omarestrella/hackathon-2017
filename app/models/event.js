import Ember from 'ember';
import DS from 'ember-data';
import moment from 'moment';

const {
    attr,
    Model
} = DS;

const {
    computed
} = Ember;

const TRUNCATE_NUMBER = 8;

export default Model.extend({
    attendees: attr(),
    organizer: attr(),

    subject: attr(),
    body: attr(),

    location: attr(),

    start: attr(),
    end: attr(),

    isAllDay: attr(),

    startDate: computed('start', function () {
        const date = this.get('start');
        return moment.tz(date.dateTime, date.timeZone).tz('America/New_York');
    }),

    endDate: computed('end', function () {
        const date = this.get('end');
        return moment.tz(date.dateTime, date.timeZone).tz('America/New_York');
    }),

    startTimeString: computed('startDate', function () {
        const date = this.get('startDate');
        return date.format('h:mm a');
    }),

    endTimeString: computed('endDate', function () {
        const date = this.get('endDate');
        return date.format('h:mm a');
    }),

    truncatedAttendees: computed('attendees.[]', function () {
        const attendees = this.get('attendees');
        if (attendees && attendees.length > TRUNCATE_NUMBER) {
            return attendees.slice(0, TRUNCATE_NUMBER);
        }
        return attendees;
    }),

    extraAttendees: computed('attendees.[]', function () {
        const attendees = this.get('attendees');
        if (attendees && attendees.length > TRUNCATE_NUMBER) {
            return attendees.length - TRUNCATE_NUMBER;
        }
        return null;
    })
});
