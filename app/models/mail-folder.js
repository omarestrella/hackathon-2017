import DS from 'ember-data';

const {
    attr,
    Model
} = DS;

export default Model.extend({
    displayName: attr(),
    parentFolderId: attr(),
    unreadItemCount: attr(),
    totalItemCount: attr()
});
