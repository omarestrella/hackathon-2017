import Ember from 'ember';
import DS from 'ember-data';

const {
    attr,
    Model
} = DS;

const {
    computed,
    Handlebars: { Utils }
} = Ember;

export default Model.extend({
    body: attr(),
    bodyPreview: attr(),
    subject: attr(),

    from: attr(),
    toRecipients: attr(),

    isRead: attr(),
    isDraft: attr(),

    sentDateTime: attr(),
    receivedDateTime: attr(),

    internetMessageId: attr(),


    senderName: computed('from.emailAddress.name', function () {
        const name = this.get('from.emailAddress.name');
        if (!name) {
            return 'Unknown Sender';
        }
        return name;
    }),

    senderEmail: computed('from.emailAddress.address', function () {
        const email = this.get('from.emailAddress.address');
        if (!email) {
            return 'Unknown Sender';
        }
        return email;
    }),

    recipientsText: computed('toRecipients.[]', function () {
        const people = this.get('toRecipients');
        if (people.length === 0) {
            return null;
        }

        const str = people.map(({ emailAddress }) => {
            return `${emailAddress.name} <${emailAddress.address}>`
        }).join(', ');

        return Ember.String.htmlSafe(Utils.escapeExpression(str));
    })
});
