import Ember from 'ember';

const {
    Component
} = Ember;

export default Component.extend({
    message: null,

    actions: {
        reply(message) {
            this.get('transitionToRoute')('email.compose', {
                queryParams: {
                    email: message.get('senderEmail'),
                    subject: `RE: ${message.get('subject')}`
                }
            });
        }
    }
});
