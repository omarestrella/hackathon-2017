import Ember from 'ember';

const {
    inject,
    Component
} = Ember;

export default Component.extend({
    auth: inject.service(),

    actions: {
        startAuth() {
            const auth = this.get('auth');
            auth.login().then(token => {
                return auth.exchangeCodeForToken(token);
            }).then(() => {
                this.get('transitionToRoute')('calendar');
            });
        }
    }
});
