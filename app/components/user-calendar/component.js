import Ember from 'ember';
import moment from 'moment';

const {
    inject,
    Component
} = Ember;

export default Component.extend({
    store: inject.service(),

    loading: false,

    events: [],

    actions: {
        clicked(event) {
            this.get('store').findRecord('event', event.id).then(record => {
                Ember.run.scheduleOnce('afterRender', this, this.set, 'currentEvent', record);
            });
        },

        joinMeeting(meeting) {
            const data = JSON.stringify({
                meetingId: meeting.get('id'),
                meetingName: meeting.get('subject')
            });
            window.parent.postMessage(data, '*');
        }
    },

    didInsertElement() {
        this._super(...arguments);

        this.set('events', []);
        this.set('loading', true);

        const startDate = moment().startOf('month').toISOString();
        const endDate = moment().endOf('month').toISOString();

        this.get('store').query('event', {
            startDateTime: startDate,
            endDateTime: endDate,
            '$top': 50,
            '$count': true,
            '$select': 'id,subject,categories,body,start,end,location,attendees,organizer,isAllDay'
        }).then(recordArray => {
            const events = recordArray.map(record => {
                return {
                    id: record.get('id'),
                    end: moment(record.get('endDate')),
                    start: moment(record.get('startDate')),
                    title: record.get('subject')
                }
            });
            this.get('events').addObjects(events);
            this.set('loading', false);
        });
    }
});
