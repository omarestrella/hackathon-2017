import Ember from 'ember';

const {
    inject
} = Ember;

export default Ember.Component.extend({
    ajax: inject.service(),

    email: null,
    subject: null,
    message: null,

    actions: {
        sendMessage() {
            const ajax = this.get('ajax');
            const { email, subject, message } = this.getProperties('email', 'subject', 'message');
            const data = {
                message: {
                    subject,
                    body: {
                        contentType: "Text",
                        content: message
                    },
                    toRecipients: [{
                        emailAddress: {
                            address: email
                        }
                    }]
                }
            }
            ajax.post('https://graph.microsoft.com/v1.0/me/sendMail', {
                data,
                dataType: 'text'
            }).finally(() => {
                const store = this.get('store');

                this.setProperties({
                    email: null,
                    subject: null,
                    message: null
                });


            });
        },

        set(key, event) {
            this.set(key, event.target.value);
        }
    }
});
