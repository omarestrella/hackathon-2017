import Ember from 'ember';

import Message from '../../models/message';

const {
    inject,
    Component
} = Ember;

export default Component.extend({
    store: inject.service(),

    messages: null,

    didInsertElement() {
        this._super(...arguments);

        const attributes = [];
        Message.eachAttribute(name => {
            attributes.push(name);
        });

        this.get('store').findAll('mail-folder').then(folders => {
            const inbox = folders.find(folder => {
                return folder.get('displayName') === 'Inbox';
            });

            this.get('store').query('message', {
                folder: inbox ? inbox.get('id') : null,
                '$top': 50,
                '$count': true,
                '$select': attributes.join(',')
            }).then(messages => {
                const seen = [];
                this.set('messages', messages.filter(message => {
                    if (!seen.includes(message.get('internetMessageId'))) {
                        seen.push(message.get('internetMessageId'));
                    } else {
                        return false;
                    }

                    return !message.get('isDraft')
                }));
            });
        });

    }
})
