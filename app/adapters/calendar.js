import Adapter from './application';

export default Adapter.extend({
    urlForFindRecord(id) {
        if (id === 'me') {
            return `${this.host}/${this.namespace}/calendar`;
        }

        return this._super(...arguments);
    }
});
