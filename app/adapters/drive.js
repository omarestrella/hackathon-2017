import Adapter from './application';

export default Adapter.extend({
    namespace: 'beta/me',
});
