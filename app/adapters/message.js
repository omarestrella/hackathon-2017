import Adapter from './application';

export default Adapter.extend({
    urlForQuery(query) {
        let url = `${this.host}/${this.namespace}/messages`;
        if (query.folder) {
            url = `${this.host}/${this.namespace}/mailFolders/${query.folder}/messages`;
            delete query.folder;
        }
        return url;
    }
});
