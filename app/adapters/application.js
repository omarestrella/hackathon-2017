import Ember from 'ember';
import DS from 'ember-data';

const {
    RESTAdapter
} = DS;

const {
    inject,
    computed
} = Ember;

export default RESTAdapter.extend({
    auth: inject.service(),

    host: 'https://graph.microsoft.com',
    namespace: 'v1.0/me',

    headers: computed('auth.accessToken', function () {
        return {
            Authorization: `Bearer ${this.get('auth.accessToken')}`
        }
    }),
})
