import Adapter from './application';

export default Adapter.extend({
    shouldBackgroundReloadRecord() {
        return false;
    },

    urlForQueryRecord() {
        return `${this.host}/${this.namespace}/calendar/calendarView`;
    }
});
