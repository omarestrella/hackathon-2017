import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
    location: config.locationType,
    rootURL: config.rootURL
});

Router.map(function () {
    this.route('calendar');
    this.route('email', function () {
        this.route('message', { path: ':message_id' });
        this.route('compose', { path: 'compose'});
    });
});

export default Router;
