import Ember from 'ember';

const {
    Controller
} = Ember;

export default Controller.extend({
    queryParams: ['email', 'subject'],

    email: null,
    subject: null
})
