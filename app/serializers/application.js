import DS from 'ember-data';

const {
    RESTSerializer
} = DS;

export default RESTSerializer.extend({
    normalizeFindRecordResponse(store, modelClass, payload, id, requestType) {
        const data = {};
        data[modelClass.modelName] = payload;
        return this._super(store, modelClass, data, id, requestType);
    },

    normalizeFindAllResponse(store, modelClass, payload, id, requestType) {
        const data = {};
        data[modelClass.modelName] = payload.value;
        return this._super(store, modelClass, data, id, requestType);
    },

    normalizeQueryResponse(store, modelClass, payload, id, requestType) {
        const data = {};
        data[modelClass.modelName] = payload.value;
        return this._super(store, modelClass, data, id, requestType);
    }
});
